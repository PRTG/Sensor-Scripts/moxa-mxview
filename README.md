# Moxa MXView

Sample Python Script Sensor to retrieve device count data from Moxa MXView

## Installation

Simply copy the MXViewCount.py file to the “Python” subfolder within the PRTG “Custom Sensors” folder (e.g. - C:\Program Files (x86)\PRTG Network Monitor\Custom Sensors\python).

Add a Python Script Advamced Sensor to the device in PRTG and choose the MXViewCount.py as the script to execute. You need to specify three values in the "additional parameters" field:

Host – leave as “https://%host” – This is the PRTG placeholder for hostname of the MXview server.
Endpoint – leave as “/api/devices” – The API endpoint being queried
Token – “xxxxxxxxx” - The API authorisation token, copied from the MXview “integration” menu.

The parameters are supplied in the form of a "Python Dictionary", so please make sure the formatting and punctuation is correct.

The sensor should now return the count of devices seen by MXView. You can assign channel limits (thresholds) to warn you if the value increases or decreases.

Additional information can be found in this blog article:
