# ___ ___ _____ ___
#| _ \ _ \_   _/ __|
#|  _/   / | || (_ |
#|_| |_|_\ |_| \___|
#    NETWORK MONITOR
#
#-------------------
# Name             MXViewCount.py
# Description      PRTG Python Script Advanced Sensor to retrieve device count from Moxa MXview  
#-------------------
# Notes
#
# - Modules "requests", "sys" and "JSON-python-module" must be installed and available to the PRTG Python instance.
# 
# - The sensor manual page can be found here - https://www.paessler.com/manuals/prtg/python_script_advanced_sensor
#
# - This A blog post explains how to setup and use the sensor -  
#
#-------------------
# Version History
# 
# Version  Date        Notes
# 1.0      11/10/2021  Initial Release
#
# ------------------
# Paessler AG
# ------------------
import requests
from requests.packages import urllib3
import json
import sys

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

from prtg.sensor.result import CustomSensorResult
from prtg.sensor.units import ValueUnit

if __name__ == "__main__":
    # Initializing some variables to make sure they are present when called
    csr = CustomSensorResult(text="")

    try:
        # Loading data dictionary passed from PRTG
        prtgParam = json.loads(sys.argv[1])
        params = json.loads(prtgParam["params"])

        # Parse PRTG Params
        apiHost = params["host"]
        apiEndpoint = params["endpoint"]
        apiToken = params["token"]
        
        apiUrl = apiHost + apiEndpoint

        payload={}
        headers = {
        'Authorization': apiToken
        }

        response = requests.request("GET", apiUrl, headers=headers, data=payload, verify=False)
        data = response.json()
        deviceCount = (len(data))

        # Create PRTG channel for discovered device count
        csr.add_primary_channel(name  ="Device Count",
            value = deviceCount,
            is_float = False)

        
    except KeyError as k:
        csr.text = "Python Script argument error"
        csr.error = f"Python Script argument error: {str(k)}"
    except Exception as e:
        csr.text = "Python Script execution error"
        csr.error = f"Python Script execution error: {str(e)}"

    finally:
        # To reduce overhead do the output in finally
        sys.stdout.write(csr.json_result)
